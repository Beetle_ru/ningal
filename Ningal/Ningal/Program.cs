﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using Microsoft.Win32;

namespace Ningal {
    internal class Program {
        public static string[] Arguments;

        public struct RegKey {
            public RegistryHive BaseKey;
            public string SubKey;
            public string Name;
        }

        public delegate void CallFunct(string value);

        public static RegKey ExplorerRegKey;

        private static void Main(string[] args) {
            Arguments = args;
            ExplorerRegKey =  new RegKey() {
                                    BaseKey = RegistryHive.LocalMachine,
                                    SubKey = @"Software\Microsoft\Windows NT\CurrentVersion\Winlogon",
                                    Name = "Shell"
                                };

            var handlers = new Dictionary<string, CallFunct>();

            handlers.Add("undefined", Help);
            handlers.Add("help", Help);
            handlers.Add("lock", LockTBL);
            handlers.Add("unlock", UnLockTBL);
            handlers.Add("recovery", Recovery);

            ProcArgs(args, handlers);
        }

        public static void LockTBL(string value) {
            var shell = value == "" ? "cmd" : value;
            RegWriteVal(ExplorerRegKey, shell);
            LockUnlock(true);
        }

        public static void UnLockTBL(string value) {
            RegWriteVal(ExplorerRegKey, "explorer.exe");
            LockUnlock(false);
        }

        public static void Help(string value) {
            Console.WriteLine("Ningal is a program for blocking/unblocking windows desktop");
            Console.WriteLine("");
            Console.WriteLine("Usage:");
            Console.WriteLine("\tNingal [options]");
            Console.WriteLine("");
            Console.WriteLine("Options:");
            Console.WriteLine("\t--lock [shell]...........Lock desktop and set default shell(empty for cmd)");
            Console.WriteLine("\t--unlock.................Unlock desktop");
            Console.WriteLine("\t--help...................Print this man");
            Console.WriteLine("\t--recovery...............Recovery desktop(if you lost desktop)");
        }

        public static void Recovery(string value) {
            UnLockTBL("");
            Process.Start("shutdown", "-r -t 1");
        }

        public static void LockUnlock(bool isLock) {
            var disO = GetDisableOptions();
            var enO = GetEnableOptions();

            ChangeAll(disO, isLock);
            ChangeAll(enO, !isLock);

            KillRunExplorer(!isLock);
        }

        public static void ProcArgs(string[] args, Dictionary<string, CallFunct> handlers) {
            if (!args.Any()) {
                CallHandler("undefined", "", handlers);
                return;
            }

            var cmd = "";
            var value = "";
            foreach (var s in args) {
                if (s.StartsWith("--")) {
                    if (s != "") CallHandler(cmd, value, handlers);
                    cmd = s;
                    value = "";
                }
                else value = s;
            }
            CallHandler(cmd, value, handlers);
        }

        public static void CallHandler(string cmd, string arg, Dictionary<string, CallFunct> handlers) {
            cmd = cmd.Replace("--", "");
            if (handlers.ContainsKey(cmd)) handlers[cmd](arg);
        }

        public static void Admin() {
            var ident = WindowsIdentity.GetCurrent();
            var isAdministrativeRight = false;
            if (ident != null) {
                var pricipal = new WindowsPrincipal(ident);
                isAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);
            }

            if (!isAdministrativeRight) {
                var processInfo = new ProcessStartInfo();
                processInfo.Verb = "runas"; //указываем, что процесс должен быть запущен с правами администратора
                processInfo.FileName = Environment.GetCommandLineArgs()[0];

                foreach (var argument in Arguments) {
                    if (processInfo.Arguments != "") processInfo.Arguments += " ";
                    processInfo.Arguments += argument;
                }

                try {
                    Process.Start(processInfo);
                }
                catch (Exception) {
                    //Ничего не делаем, потому что пользователь, возможно, нажал кнопку "Нет" в ответ на вопрос о запуске программы в окне предупреждения UAC (для Windows 7)
                }
                Environment.Exit(0);
            }
        }

        public static Dictionary<string, RegKey> GetDisableOptions() {
            var oa = new Dictionary<string, RegKey>();

            oa.Add("DisableChangePassword",
                   new RegKey() {
                                    BaseKey = RegistryHive.CurrentUser,
                                    SubKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System",
                                    Name = "DisableChangePassword"
                                });

            oa.Add("DisableLockWorkstation",
                   new RegKey() {
                                    BaseKey = RegistryHive.CurrentUser,
                                    SubKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System",
                                    Name = "DisableLockWorkstation"
                                });

            oa.Add("NoLogoff",
                   new RegKey() {
                                    BaseKey = RegistryHive.CurrentUser,
                                    SubKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System",
                                    Name = "NoLogoff"
                                });

            oa.Add("DisableTaskMgr",
                   new RegKey() {
                                    BaseKey = RegistryHive.CurrentUser,
                                    SubKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System",
                                    Name = "DisableTaskMgr"
                                });

            oa.Add("HideUserSwitching", // error, moove to GetEnableOptions
                   new RegKey() {
                                    BaseKey = RegistryHive.LocalMachine,
                                    SubKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System",
                                    Name = "HideFastUserSwitching"
                                });

            oa.Add("ShutdownWithoutLogon",
                   new RegKey() {
                                    BaseKey = RegistryHive.LocalMachine,
                                    SubKey = @"Software\Microsoft\Windows\CurrentVersion\Policies\System",
                                    Name = "ShutdownWithoutLogon"
                                });

            return oa;
        }

        public static Dictionary<string, RegKey> GetEnableOptions() {
            var oa = new Dictionary<string, RegKey>();

            oa.Add("DisableChangePassword",
                   new RegKey() {
                                    BaseKey = RegistryHive.LocalMachine,
                                    SubKey = @"Software\Microsoft\Windows NT\CurrentVersion\Winlogon",
                                    Name = "AutoRestartShell"
                                });

            return oa;
        }

        public static void RegOption(RegKey key, bool isEnabled) {
            var value = isEnabled ? 1 : 0;
            RegWriteVal(key, value);

        }

        public static void RegWriteVal(RegKey key, object val) {
            Admin();

            var ourKey = RegistryKey.OpenBaseKey(key.BaseKey,
                                                 Environment.Is64BitOperatingSystem
                                                     ? RegistryView.Registry64
                                                     : RegistryView.Registry32);
            ourKey = ourKey.CreateSubKey(key.SubKey);

            if (ourKey != null) {
                var regval = ourKey.GetValue(key.Name);
                if ((val != null) && ((regval == null) || (regval != val)))
                    ourKey.SetValue(key.Name, val);
            }
        }

        public static void ChangeAll(Dictionary<string, RegKey> oa, bool value) {
            foreach (var regKey in oa) {
                RegOption(regKey.Value, value);
                Console.WriteLine("{0} = {1}", regKey.Key, value);
            }
        }

        public static void KillRunProcesses(bool isRun, string name) {
            var explorers = Process.GetProcessesByName(name);

            if (!isRun) {
                foreach (var process in explorers)
                    process.Kill();
            }
            else if (!explorers.Any()) Process.Start(name);
        }

        public static void KillRunExplorer(bool isRun) {
            const string exploreName = "explorer";
            var explorers = Process.GetProcessesByName(exploreName);

            if (!isRun)
                KillRunProcesses(false, exploreName);
            else {
                if (!explorers.Any()) {
                    var windir = Environment.GetEnvironmentVariable("windir");
                    if (windir != null) Process.Start(Path.Combine(windir, exploreName + ".exe"));
                }
            }
        }
    }
}